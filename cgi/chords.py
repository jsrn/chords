#!/usr/bin/python
# -*- coding: utf-8 -*-

# This file is specialised for the system on DTU
import cgi
import subprocess
import sys
import os
import datetime

# DTU-setup
latex_path = "../../latex/texlive/2013/bin/x86_64-linux"
os.environ["PATH"] += ":"+latex_path
chord_dir = "chords"
song_path = "../songs"

#############################
#CGI Frontend to chords.py
#############################

def html_tag(tag, options=None, hclass=None, contents=None):
    str_builder = []
    str_builder.append("<")
    str_builder.append(tag)
    if hclass:
        str_builder.append(" class=\"")
        str_builder.append(hclass)
        str_builder.append("\"")
    if options:
        str_builder.append(" ")
        str_builder.append(options)
    if contents:
        str_builder.append(">")
        str_builder.append(contents)
        str_builder.append("</%s>" % tag)
    else:
        str_builder.append("/>")
    return "".join(str_builder)

def html_list(hclass=None, elements=[]):
    return html_tag("ul", hclass=hclass, contents=\
       "\n".join( html_tag("li", contents=el) for el in elements ))

### FIXED HTML PARTS
content_type = r"""Content-Type: text/html; charset=utf-8"""

html_header = r"""
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Typeset chorded lyrics beautifully</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
"""

html_body_starter = r"""
<script id='example_song' type='text/plain'>Negro Spiritual
Pick a bale of cotton

Capo: 2nd

A#
Jump down, turn around to pick a bale of cotton
                          F                  A#
Jump down, turn around to pick a bale a day.
Jump down, turn around to pick a bale of cotton
                          F                  A#
Jump down, turn around to pick a bale a day.

Chorus:
D#        A#                     D#
Oh Lordy, pick a bale of cotton, oh Lordy,
F             A#
Pick a bale a day.
D#        A#                     D#
Oh Lordy, pick a bale of cotton, oh Lordy,
F             A#
Pick a bale a day.

Me and my gal can pick a bale of cotton,
Me and my gal can pick a bale a day.

Chorus

Me and my wife can pick a bale of cotton,
Me and my wife can pick a bale a day.

Chorus

Me and my friend can pick a bale of cotton,
Me and my friend can pick a bale a day.

Chorus

Me and my papa can pick a bale of cotton,
Me and my papa can pick a bale a day.
</script>
<script type=text/javascript>
function fillExample() {
   var inp = document.getElementById('lyrics');
   var example = document.getElementById('example_song');
   inp.value = example.textContent;
}
</script>
</head>
<body>
<div class="top">
<h1><span class="bigletter">T</span>ypeset chorded lyrics <span class="bigletter">B</span>eautifully</h1>
</div>
"""

html_form_header = r"""
<form action="chords.py" method="post" accept-charset="utf-8">
"""
html_form_footer = "</form>"

html_footer = r"""
<div class="about">
<p class="about">
The program is written in Python and uses LaTeX + pdfLaTeX as backend to
typeset the document. The code is released under GPL v3 and is <a href="https://bitbucket.org/jsrn/chords">hosted at Bitbucket</a>.
</p>
<p class="signature">Johan S. R. Nielsen<span>&nbsp;</span><a href="http://jsrn.dk">www.jsrn.dk</a>
<p class="contact">E-mail: "chords" and then an at-symbol and then "jsrn.dk".</p>
<p>Last updated %s.</p>
</div>
</body>
</html>
""" % datetime.date.today()

def html_body_err(exc_name, exc_info):
    return r"""
</head>
<body>
<div class=\"error\">A critical error has occured: %s\n\tMessage: %s
</body>
</html>""" % (exc_name, exc_info)

### SMALLER TEXTS
err_text_unknown = """An unknown error prevented typesetting of the pdf file.

                         Often, this is due to certain special characters that
                         is unsupported by LaTeX. Check your song text to be
                         sure that there are no such charcters.

                         If this is not the case, please send an email to the
                         developer, along with a copy of the lyrics you are
                         trying to typeset."""
err_latex = "I encountered an error while trying to understand your lyrics file!"
text_lyrics_title = """Paste in your chorded lyrics"""
text_options_title = """Select options"""
text_submit_title = """Press here"""
text_question_title = "Need Help"
text_sizemax_desc = "Maximise text-size to fit on pages"
text_sizerel_desc = "Text-size (-5,...,5 and 0 is normal)"
text_columns_desc = "2 columns"
text_margins_desc = "Margins:"


### CSS CLASSES
class_err = "err"
class_err_title = "err_title"
class_err_msg= "err_msg"
class_err_warning = "err_warning"
class_input_area = "input_area"
class_option_area = "option_area"
class_submit_area = "submit_area"
class_question_area = "question_area"
class_option_selection = "selection"
class_option_desc = "option_desc"
class_option_list = "option_list"
class_option_radio = "option_radio"
class_option_checkbox = "option_checkbox"
class_input_text = "input_text"
class_question_box = "question_box"

### HTML TAGS
tag_area_title = "p"
tag_textarea ="textarea"
tagoptions_textarea = """name="lyrics" id="lyrics" """
tag_err_title= "p"
tag_err_msg = "pre"
tag_option_desc = "span"
tag_submit = """input type="submit" value="Typeset" """


### SPECIFIC HTML GENERATORS
def html_area_header(number, title):
    return html_tag("div", hclass="area_header", contents=\
              html_tag("div", hclass="area_number", contents=str(number)+")") \
            + html_tag("div", hclass="area_title", contents=title))

def html_radio(group, value, checked):
   return html_tag("input", hclass=class_option_radio, options=\
            """type="radio" name="%s" value="%s" %s""" \
                    % (group, value, "checked=\"checked\"" if checked else ""))

def html_checkbox(group, value, checked):
   return html_tag("input", hclass=class_option_checkbox, options=\
            """type="checkbox" name="%s" value="%s" %s""" \
                    % (group, value, "checked=\"checked\"" if checked else ""))

def html_input_small(name, value, id=None):
    return html_tag("input", hclass=class_input_text, options=\
                    """type="text" name="%s" size="2" value="%s" %s""" % (name, value, "id=\"%s\"" % id if id else ""))


def preHandleLyrics(lyrics):
    """Preprocess the lyrics for various superficial syntax errors"""
    warnings = []
    if lyrics.find('\t') > -1:
        warnings.append("The lyrics contain tabs; consider not using these as their space width will differ from program to program. Here they will be considered 4 spaces")
        lyrics = lyrics.replace("\t","    ")
    return (lyrics, warnings)

#Probably too old Python for this
#cgitb.enable()
print content_type     # HTML is following
print                               # blank line, end of headers
print html_header

#try:
if True:
    form = cgi.FieldStorage()

    options = dict()
    try:
        if form["size"].value == "sizemax":
            options["sizemax"] = True
            options["sizerel"] = False
        else:
            options["sizemax"] = False
            options["sizerel"] = True
    except KeyError:
        options["sizemax"] = False
        options["sizerel"] = True

    try:
        if form["columns"].value == "columns":
            options["columns"] = True
        else:
            options["columns"] = False
    except KeyError:
        options["columns"] = False

    def loadNumOption(name, default, numf):
        try:
            options[name] = numf(form[name].value)
        except (ValueError, KeyError):
            options[name] = default
    loadNumOption("sizemaxN", 1, int)
    loadNumOption("sizerelN", 0, int)
    loadNumOption("margin_top", 1.2, float)
    loadNumOption("margin_bot", 1.5, float)
    loadNumOption("margin_left", 1.0, float)
    loadNumOption("margin_right", 1.0, float)

    errs = None
    fileErr = False
    warnings = None
    if "lyrics" in form:
        TMP_FILE = "song.pdf"
        params = ['./chords.py','-o',TMP_FILE]
        if options["sizemax"]:
            params.append("--max")
            params.append(str(options["sizemaxN"]))
        else:
            params.append("--size")
            params.append(str(options["sizerelN"]))
        if options["columns"]:
            params.append("--cols")
        params.append("--margins")
        params.append(str(options["margin_top"]))
        params.append(str(options["margin_bot"]))
        params.append(str(options["margin_left"]))
        params.append(str(options["margin_right"]))
        params.append("--safemode")
        proc = subprocess.Popen(params,
                        cwd=chord_dir,
                        stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        (lyrics, warnings) = preHandleLyrics(form["lyrics"].value)
        (dummy,errs)= proc.communicate(input=lyrics)
        if not errs:
            #print "onload=downloadURL(\"%s/%s\");" % (chord_dir, TMP_FILE)
            proc = subprocess.Popen(['mv', chord_dir+"/"+TMP_FILE, song_path+"/"+TMP_FILE],
                        stdin=subprocess.PIPE, stderr=subprocess.PIPE)
            (dummy,errs)= proc.communicate()
            if errs:
                fileErr = True

    #MAIN HEADER
    print "<style type=\"text/css\">\n"
    f = open("chords.css")
    print f.read()
    f.close()

    print  r"""</style>"""

    #DOWNLOADER
    if "lyrics" in form and not errs:
        print "<meta http-equiv=\"refresh\" content=\"0;url=%s/song.pdf\" />" % song_path

    # REMAINING HEADER
    print html_body_starter 
#except: #If something in parsing goes wrong, inform user
#    print html_body_err(sys.exc_info()[0], sys.exc_info()[1]) 

#No errors, continuing
#try:
if True:
    #ERROR BOX
    if "lyrics" in form:
        if errs:
            err_text = err_text_unknown if fileErr else err_latex
            print html_tag("div", hclass=class_err, contents=\
                    html_tag(tag_err_title, hclass=class_err_title, contents=err_text) \
                  + "\n" \
                  + html_tag(tag_err_msg, hclass=class_err_msg, contents=errs))
        elif warnings:
            warning_tags = "\n".join( html_tag(tag_err_msg,hclass=class_err_title,contents=w) \
                                              for w in warnings )
            print html_tag("div",hclass=class_err_warning,contents=warning_tags)
    #### FORM
    print html_form_header
    # Input area
    print html_tag("div", hclass=class_input_area, contents=\
            html_area_header(1, text_lyrics_title) \
          + html_tag(tag_textarea, options=tagoptions_textarea, \
                         contents= form["lyrics"].value if "lyrics" in form else "\n") )
    # Options area
    print html_tag("div", hclass=class_option_area, contents=\
            html_area_header(2, text_options_title) \
          + html_list(hclass=class_option_list, elements =
              [
                # Radio buttons for size (one list element)
                html_tag("div", hclass=class_option_selection, contents=\
                    html_radio("size","sizerel",options["sizerel"]) \
                    + html_tag(tag_option_desc, hclass=class_option_desc, contents=text_sizerel_desc) \
                    + html_input_small("sizerelN", options["sizerelN"]))
              + html_tag("div", hclass=class_option_selection, contents=\
                  html_radio("size","sizemax",options["sizemax"]) \
                  + html_tag(tag_option_desc, hclass=class_option_desc, contents=text_sizemax_desc) \
                  + html_input_small("sizemaxN", options["sizemaxN"]))
                  
              , html_tag("div", hclass=class_option_selection, contents=\
                  html_checkbox("columns", "columns", options["columns"])\
                  + html_tag(tag_option_desc, hclass=class_option_desc, contents=text_columns_desc))

              , html_tag("div", hclass=class_option_selection, contents=\
                  html_tag(tag_option_desc, hclass=class_option_desc, contents=text_margins_desc)
                + html_tag("div", hclass="margin_box", contents=\
                    html_input_small("margin_top",options["margin_top"], id="margin_top")
                  + html_input_small("margin_left",options["margin_left"], id="margin_left")
                  + html_input_small("margin_right",options["margin_right"], id="margin_right")
                  + html_input_small("margin_bot",options["margin_bot"], id="margin_bot")))
              ]))
    # Submit area
    print html_tag("div", hclass=class_submit_area, contents=\
            html_area_header(3, text_submit_title) \
          + html_tag(tag_submit) )

    # Question area
    print html_tag("div", hclass=class_question_area, contents=\
            html_area_header("?", text_question_title) \
          + html_tag("div", hclass=class_question_box, contents=\
              html_tag("a", contents="See an example", options="onclick=\"fillExample()\"")
            #+ html_tag("span", contents="or")
            #+ html_tag("a", contents="Read more")
                     ))

    print html_form_footer
    print html_footer
#except: #If something in value writing went wrong, inform
#    print html_body_err(sys.exc_info()[0], sys.exc_info()[1])
