tests: chords.py song.py test/*.song
	-mkdir test_out
	-mkdir test_out/test
	for t in test/*.song; do \
	    if grep "#TEST-SKIP" $$t >/dev/null; then \
	        echo Skipping $$t; \
	    else\
	        echo Compiling $$t;\
		./chords.py -o test_out/$$t.pdf `sed -n 's/#FLAGS:\(.*\)/\1/p' < $$t` $$t; \
	    fi \
	done
	pdfunite test_out/test/*pdf tests.pdf
	rm -rf test_out

test2:
	./chords.py -o test.pdf test2.song

clean:
	rm -rf *.{log,aux} test.pdf test.aux test.log chords.py.tmp.tex texput.log

.PHONY:cgi
cgi:
	cd cgi; scss chords.scss:chords.css; cd ..
	#date=`date`; sedstring='{s/UPDATED_DATE/'$$date'/g}'; sed "$$sedstring" <  cgi/chords.raw.py > cgi/chords.py

.PHONY:html
html: cgi
	cd cgi; ./chords.py > test.html
