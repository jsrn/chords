#!/usr/bin/python3

import codecs
import sys
import argparse
import string
import subprocess
import os
import re

from song import *

def getScriptDir():
    import os
    return os.path.dirname(os.path.realpath(__file__))

def pdfPageCount(texFileName):
    """Return the number of pages in a pdf file."""
    logFile = codecs.open(texFileName[:-3] + "log", 'r', encoding="iso-8859-1")
    line = logFile.readline()
    regex = re.compile('Output written on .*\((\d*) page.')
    match = None
    while line != '':
        match = regex.match(line)
        if match:
            break
        line = logFile.readline()
    if line == '' or not match:
        raise Exception("No 'Output writen' line in TeX log file. Compilation failed?")
    return int(match.group(1))

def texCreateFile(args):
    if args.tex:
        fileName = args.outputfile
    else:
        fileName = sys.argv[0] + ".tmp.tex"
    texFile = codecs.open(fileName,'w','utf-8')
    return (texFile, fileName)

def texHeader(texFile, options):
    texFile.write("\\documentclass[10pt,a4paper]{article}\n")
    if options["cols"]:
        texFile.write("\\usepackage{multicol}\n\\raggedcolumns\n")
    texFile.write("\\usepackage{chords}\n")
    margins = options["margins"]
    texFile.write("\\usepackage[top=%fcm,bottom=%fcm,innermargin=%fcm,outermargin=%fcm]{geometry}\n" % (margins[0], margins[1], margins[2], margins[3]))
    texFile.write("\\songdocumentheader\n")
    if options["raggedright"]:
        texFile.write("\\raggedright\n")

def texFooter(texFile):
    texFile.write("\\end{document}\n")

def texCompile(texFileName, outputFile):
    texOutFileName = texFileName[:-3] + "pdf"
    proc = subprocess.Popen(['pdflatex','-interaction=nonstopmode',texFileName],
                            cwd=getScriptDir(),
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    (outb,errs) = proc.communicate()
    out = str(outb)
    #TODO: If TeX meets an error, the entire program halts
    if errs or out.find("Fatal error") > 0:
        if errs:
            sys.stderr.write("pdfLaTeX returned error: %s\n" % errs)
        else:
            sys.stderr.write("pdfLaTeX returned error: %s\n" % out[out.find("rror")-1:])
        return
    subprocess.call(['cp',texOutFileName, outputFile])

def texRemoveFile(texFile):
    #print "TODO: Delete tex file"
    pass

def texNewPage(texFile):
    texFile.write("\n\\newpage\n\n")


### Program entry
parser = argparse.ArgumentParser(description='Typeset songs with chords')
parser.add_argument('songfiles', metavar='song-file', nargs='*',
    type=argparse.FileType('r'),
    help='input song files')
parser.add_argument('-o', dest='outputfile', type=str,
    help='output pdf file (or tex file if --tex is used). Required except if --proof is used.')
parser.add_argument('--proof', dest='proof', action='store_true',
    help='Only parse and output the song source in in-line chord mode')
parser.add_argument('--tex', dest='tex', action='store_true',
    help='Produce only the TeX source file without compiling it')
parser.add_argument('--cols', dest='cols', type=int,
    help='Typeset the song with two columns')
parser.add_argument('--max', dest='max', type=int,
    help='Find the maximal font size such that the document will be at most N pages.')
parser.add_argument('--size', dest='size', type=int,
    help='Set the relative font size (integer, usually between -5 and 5)')
parser.add_argument('--safemode', dest='safemode', action='store_true',
    help='Disallow all markup which could be potentially dangerous, in particular custom TeX.')
parser.add_argument('--margins', dest='margins', nargs=4, type=float,
    help='Set the paper margins in cm. 4 space-separated floats: top, bottom, left, right.')
#NOTE: songfiles are opened by argparse while the outputfile is not

args = parser.parse_args()
options = dict()
options['cols'] = args.cols
options['raggedright'] = True
if args.size:
    options['size'] = args.size
else:
    options['size'] = 0
if args.margins:
    options['margins'] = args.margins
else:
    options['margins'] = [ 1.5, 1.2, 1, 1 ]

# Parse all songs
try:
    if args.songfiles:
        songs = []
        for songfile in args.songfiles:
            try:
                songs.append(Song(songfile))
            except:
                sys.stderr.write("An error occurred while trying to understand %s:\n" % songfile)
                raise
    else:
        try:
            songs = [ Song(sys.stdin) ]
        except: 
            sys.stderr.write("An error occurred while trying to understand the song:\n")
            raise
except SongError as e:
    sys.stderr.write("%s\n\n" % e.getMessage())
    sys.stderr.write("The error happened\n")
    sys.stderr.write(e.format_info_stack())
    sys.stderr.flush()
    sys.exit(1)
except Exception:
    sys.stderr.write("""
The error was a completely unexpected internal error that I do not know how pinpoint.
This should never happen; please send an email to the developer along with the song you tried to typeset.\n""")
    sys.stderr.flush()
    sys.exit(1)

if args.safemode:
    for song in songs:
        if not song.is_safe():
            sys.stderr.write("The song '%s' contains potentially dangerous markup which is disallowed.\n" % song.title)
            sys.stderr.flush()
            sys.exit(1)

if args.proof:
    # Just re-output the songs
    if args.outputfile:
        out = open(fileName,'w')
    else:
        out = sys.stdout
    for song in songs:
        print(song)
        print("\n")

else:
    if not args.outputfile:
        print("Error: You did not specify an output file (flag -o)")
        sys.exit(1)

    # Create a Pdf or TeX document
    if not args.tex:
        outputFile = args.outputfile

    def createDocument():
        pass
        (texFile, texFileName) = texCreateFile(args)
        texHeader(texFile, options)
        first = True
        for song in songs:
            if not first:
                texNewPage(texFile)
            first = False
            song.toTexFile(texFile, options)

        texFooter(texFile)
        texFile.close()

        if not args.tex:
            texCompile(texFileName, outputFile)
        #    texRemoveFile(texFileName)

        return texFileName

    if not args.max:
        createDocument()
    else:
        # Try different sizes until we find the maximal allowing the document to be args.max pages long
        minr, maxr = -5, 5   
        failed=True
        while minr < maxr:
            tryr = minr + ((maxr-minr) - (maxr-minr)//2)
            options["size"] = tryr
            texFileName = createDocument()
            if pdfPageCount(texFileName) > args.max:
                maxr = tryr-1
                failed=True
            else:
                minr = tryr
                failed=False
        if failed:
            options["size"] = minr 
            createDocument()

# vim: tw=0
