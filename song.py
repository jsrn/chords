#Module for Songs
# coding=latin1
import string

#TODO: Building the TeX string could be done more efficiently

class SongError(Exception):
    def __init__(self):
        self.info_stack = []# a list of list of lines, each inner list repr an error recog point
    def format_info_stack(self):
        str_builder  = []
        for i in range(0, 5):
            for line in self.info_stack[i]:
                str_builder.append("%s%s\n" % ("\t"*i, line))
        return "".join(str_builder)
    def getMessage(self):
        return self.message

class NoTitleArtistError(SongError):
    def __init__(self):
        SongError.__init__(self)
    def getMessage(self):
        return "The title and artist of the song must be specified at the top of the song with a line each" 

class UnmatchedParenError(SongError):
    def __init__(self, line, pos):
        SongError.__init__(self)
        self.line = line
        self.pos = pos
    def getMessage(self):
        return "The chord parenthesis initiated at position %s was unmatched:\n\t%s" % (self.pos, self.line)

class InvalidLeadingBaseNote(SongError):
    def __init__(self, note):
        SongError.__init__(self)
        self.note = note
    def getMessage(self):
        return "This is not a real chord letter: %s" % self.note

class ExpectedEndofChord(SongError):
    def __init__(self, chord, pos):
        SongError.__init__(self)
        self.chord = chord
        self.pos = pos
    def getMessage(self):
        return "I understand only the '%s' part of the chord '%s'" % (self.chord[:self.pos], self.chord)

class UnmatchedTexError(SongError):
    def __init__(self):
        SongError.__init__(self)
    def getMessage(self):
        return "TeX parenthesis unmatched"


class UnexpectedParsingError(SongError):
    def __init__(self, discovered_at):
        SongError.__init__(self)
        self.discovered_at = discovered_at
    def getMessage(self):
        return "While parsing the song, I encountered an unexpected error. Please inform the developer of this site!\nThe error was discovered during %s" % self.discovered_at

exc_unmatched_tex = "TeX block unmatched"

#Tex bits
def tex_math(s):
    return "\\begin{math}"+ s +"\\end{math}"
TEX_FLAT=tex_math("^\\flat")
TEX_SHARP=tex_math("^\\sharp")


class Chord:
    def __init__(self, chordString):
        # Parse the chord string 
        try:
            ret = Chord.parse(chordString)
        except SongError as e: 
            e.info_stack.append(["While trying to understand chord: %s" % chordString])
            raise
        except Exception as e:
            raise UnexpectedParsingError("interpreting '%s', a chord" % chordString) 
        if ret:
            (baseNote, leadingNo, dec, trailNo, botNote) = ret
            self.baseNote = baseNote 
            self.leadingNo = leadingNo
            self.dec = dec
            self.trailNo = trailNo
            self.botNote = botNote
        else:
            self.error = True
            self.chordString = chordString

    def toTex(self):
        def texNote(note):
            if note:
                if len(note)==2:
                    if note[1]=="b":
                        return texNote(note[0]) + TEX_FLAT 
                    else:
                        return texNote(note[0]) + TEX_SHARP
                return note
            return ""
        def texNo(no):
            if no:
                return tex_math("^\\chordmodtext{%s}"% no)
            return ""
        def texDec(dec):
            if dec:
                if dec=="m":
                    return dec
                elif dec=="mmaj":
                    return "m"+tex_math("^\\chordmodtext{maj}")
                else:
                    return tex_math("^\\chordmodtext{"+dec+"}")
                return dec
            return ""
        def texBotNote(botNote):
            if botNote:
                return tex_math("/")+texNote(botNote)
            return ""
        return "\\chordtext{%s}" % (texNote(self.baseNote)+texNo(self.leadingNo)+texDec(self.dec)+texNo(self.trailNo)+texBotNote(self.botNote))

    def __str__(self):
        def strIt(s):
            if s:
                return str(s)
            return ""
        def strBotNote(botNote):
            if botNote:
                return "/"+botNote
            return ""
        return strIt(self.baseNote)+strIt(self.leadingNo)+strIt(self.dec)+strIt(self.trailNo)+strBotNote(self.botNote)
        
    
    @staticmethod
    def parse(word):
        """Heuristic for parsing a chord"""
        #TODO: This is probably not complete or accurate
        # Chords have this structure:
        #   <note>[N][dec][N][/<note>]
        # where a note is
        #   <Chord letter>[# or b]
        # and dec is one of 
        #   m,maj,mmaj,min,dim,aug,sus,add,-
        # and N is an integer in [1;13]
        length = len(word)
        def sliceNote(i):
            if i >= length:
                return None
            if word[i] in ['A','B','C','D','E','F','G','H']:
                if i+1 < length and word[i+1] in ['#','b']:
                    return (i+2, word[i:i+2])
                return (i+1, word[i])
            return None
        def sliceNumber(i):
            #Return (j,N) if word[i:j] = "N" where N is a no. else return None
            j = i
            while j<length and word[j] in ['0','1','2','3','4','5','6','7','8','9']:
                j=j+1
            if j>i:
                return (j, int(word[i:j]))
            else:
                return None
        def sliceDec(i):
            if i >= length:
                return None
            #First guess its one of the three-letter ones 
            if i+3 <= length and word[i:i+3] in ["maj","min","dim","aug","sus","add"]:
                return (i+3, word[i:i+3])
            elif i+4 <= length and word[i:i+4] == "mmaj":
                return (i+4, word[i:i+4])
            elif i < length and word[i] in ['m','-']:
                return (i+1, word[i])
            return None
        def sliceBot(i):
            if i >= length:
                return None
            if word[i] == "/":
                ret = sliceNote(i+1)
                if ret:
                    return ret
            return None

        (leadingNo, dec, trailNo, botNote) = (None,None,None,None)
        i = 0
        ret = sliceNote(i)
        if not ret:
            raise InvalidLeadingBaseNote(word[i])
        (i, baseNote) = ret

        ret = sliceNumber(i)
        if ret:
            (i, leadingNo) = ret

        ret = sliceDec(i)
        if ret:
            (i, dec) = ret

        ret = sliceNumber(i)
        if ret:
            (i, trailNo) = ret

        ret = sliceBot(i)
        if ret:
            (i, botNote) = ret

        if i<length:
            raise ExpectedEndofChord(word, i)

        return (baseNote, leadingNo, dec, trailNo, botNote)

    @staticmethod
    def isChord(word):
        try:
            Chord.parse(word)
        except SongError:
            return False
        return True
        
        


class LinePiece: #Abstract
    def toTex(self):
        raise NotImplementedError("Abstract method")

class StringPiece(LinePiece):
    def __init__(self, string):
        self.string = string
    def toTex(self):
        return texString(self.string)
    def __str__(self):
        return self.string
    def ends_with_space(self):
        return self.string[-1] == ' '
    def begins_with_space(self):
        return self.string[0] == ' '

class ChordPiece(LinePiece):
    def __init__(self, chordString):
        self.chord = Chord(chordString)
    def toTex(self):
        return self.chord.toTex()
    def __str__(self):
        return "(%s)" % self.chord

class TexPiece(LinePiece):
    def __init__(self, tex):
        self.tex = tex
    def toTex(self):
        return "{%s}" % self.tex
    def __str__(self):
        return "{|%s|}" % self.tex

class VerseLine(object): #Abstract
    def toTex(self):
        raise NotImplementedError("Abstract method")

class PiecesLine(VerseLine):
    def __init__(self, pieces):
        self.pieces = pieces
    def toTex(self):
        texs = [ piece.toTex() for piece in self.pieces ]
        strs = []
        i=0
        length = len(texs)
        while i<length:
            if isinstance(self.pieces[i], ChordPiece):
                if i+1<length and not isinstance(self.pieces[i+1], ChordPiece):
                    if isinstance(self.pieces[i+1], StringPiece) and self.pieces[i+1].begins_with_space():
                        strs.append(" ")
                    underText = texs[i+1]
                    strs.append("\\chordovertext{%s}{%s}" % (texs[i], underText))
                    if isinstance(self.pieces[i+1], StringPiece) and self.pieces[i+1].ends_with_space():
                        strs.append(" ")
                    i += 2
                else:
                    strs.append("\\chordovertext{\\spacebeforeemptychord%s}{\\ }" % texs[i])
                    i += 1
            else:
                strs.append(texs[i])
                i += 1
        return "".join(strs)
    def __str__(self):
        return "".join([ str(piece) for piece in self.pieces ])
    def is_safe(self):
        for piece in self.pieces:
            if isinstance(piece, TexPiece):
                return False
        return True

class ChordLine(VerseLine):
    def __init__(self, chordLineStr):
        chordStrings = chordLineStr.split()
        self.chords = [ Chord(chordStr) for chordStr in chordStrings ]
    def toTex(self):
        chordsTex = " ".join([ "%s"% c.toTex() for c in self.chords ])
        return chordsTex
    def __str__(self):
        return "play: "+"  ".join( str(c) for c in self.chords)
    def is_safe(self):
        return True

class CommentLine(VerseLine):
    def __init__(self, text):
        self.text = text
    def toTex(self):
        return "\\commenttext{(%s)}" % texString(self.text)
    def __str__(self):
        return "(%s)" % self.text
    def is_safe(self):
        return True


class SongPart: #Abstract
    def toTex(self):
        raise NotImplementedError("Abstract method")

class TexPart(SongPart):
    def __init__(self, tex):
        self.tex = tex
    def toTex(self):
        return self.tex
    def __str__(self):
        return "{|"+tex+"|}"
    def is_safe(self):
        return False

class Verse(SongPart):
    def __init__(self, lines):
        self.lines = lines
    def toTex(self):
        texLines = [ line.toTex() for line in self.lines ]
        return "\\verse{\n%s\n}" % ("\;\n".join(texLines))
    def __str__(self):
        return "\n".join([ str(l) for l in self.lines ])
    def is_safe(self):
        for line in self.lines:
            if not line.is_safe():
                return False
        return True

class ChorusDefinition(SongPart):
    def __init__(self, verse):
        self.verse = verse
    def toTex(self):
        return "\\defchorus{\n%s\n}" % self.verse.toTex()
    def __str__(self):
        return "def-chorus:\n%s" % self.verse
    def is_safe(self):
        return self.verse.is_safe()

class Chorus(SongPart):
    def toTex(self):
        return "\\chorus"
    def __str__(self):
        return "chorus"
    def is_safe(self):
        return True

class CommentPart(SongPart):
    def __init__(self, text):
        self.text = text
    def toTex(self):
        return "\\commenttext{(%s)}" % texString(self.text)
    def __str__(self):
        return "(%s)" % self.text
    def is_safe(self):
        return True





class Song:
    #Fields:
    #artist : string
    #title  : string
    #capo   : int or None
    #chords : Chord list
    #parts  : SongPart list

    def __init__(self, inputStream):
        self.parseStream(inputStream)

    def __str__(self):
        s = "Artist: %s\nTitle: %s\n" % (self.artist, self.title)
        if not self.capo is None:
            s = s + "Capo: %s\n" % self.capo
        s = s + "\n" + "\n\n".join([str(p) for p in self.parts])
        return s

    def toTexFile(self, fobj, options):
        headers=[]
        footers=[]
        def addEnvironment(begin, end):
            headers.append(begin)
            footers.append(end)
        addEnvironment("\\begin{song}{%s}{%s}" % (texString(self.title), texString(self.artist)),
                       "\\end{song}")
        if options["cols"]:
            addEnvironment("\\begin{multicols}{2}","\\end{multicols}")
        headers.reverse()

        if options.get("size",0):
            fobj.write("\\relsize{%d}\n" % options["size"])
        while headers:
            fobj.write(headers.pop())
            fobj.write('\n')
        if not self.capo is None:
            fobj.write("\n\\capotext{%s}\n\n" % texString(self.capo))
        for part in self.parts:
            fobj.write(part.toTex())
            fobj.write("\n\n")
        while footers:
            fobj.write(footers.pop() + "\n")

    def is_safe(self):
        for part in self.parts:
            if not part.is_safe():
                return False
        return True

    def parseStream(self, inputStream):
        def getBlocks():
            blocks = []
            curBlock = []
            for line in inputStream:
                linet = line.rstrip()
                if linet == "":
                    if curBlock != []:
                        blocks.append(curBlock)
                        curBlock = []
                else:
                    if linet[0] != "#":  #We interpret all lines beginning with # as a source comment (not a song comment)
                        curBlock.append(linet)
            if curBlock != []:
                blocks.append(curBlock)
            return blocks

        def extractMeta(metablock):
            if len(metablock) < 2:
                raise NoTitleArtistError()
            def getContentLine(name):
                # Find a line of the form "<name>:..."
                lname = len(name)
                for line in metablock:
                    if line[:lname].lower() == name:
                        return line[lname+1:].lstrip()
                return None
            self.title  = getContentLine("title")
            if self.title is None:
                #Positional title and artist
                self.artist = metablock[0]
                self.title = metablock[1]
            else:
                #Titled title and artist
                self.artist = getContentLine("artist")
                if self.artist is None:
                    self.artist = getContentLine("author")
                    if self.artist is None:
                        raise NoTitleArtistError()
            # Capo is never positional
            self.capo = getContentLine("capo")

        def parseLinePieces(block, curLine):
            """Parse a single line into its parts. Note that this might span
            several lines due to embedded TeX code. We continue until we reach
            the end of a line and we are not in TeX"""
            try:
                parts = []
                (last, cur) = (0,0)
                line = block[curLine]
                l = len(line)
                while cur < l:
                    if line[cur] == '(':
                        end = line.find(")",cur+1)
                        if end == -1:
                            raise UnmatchedParenError(curLine, cur)
                        if cur > last:
                            parts.append(StringPiece(line[last:cur]))
                        if end > cur+1:
                            parts.append(ChordPiece(line[cur+1:end]))
                        last = end+1
                        cur = end+1
                    elif line[cur:cur+2] == "{|":
                        #This might end only in a later line. We cheat and match
                        #its ending without actual parsing
                        end = -1
                        start = cur+1
                        nlines = len(block)
                        endLine = curLine
                        while end == -1 and endLine < nlines:
                            end = block[endLine].find("|}", start)
                            if end == -1:
                                endLine = endLine+1
                                start = 0
                        if endLine == nlines:
                            raise UnmatchedTexError() #TODO: Improve err
                        if endLine == curLine:
                            tex = line[cur+2:end]
                        else:
                            tex = block[curLine][cur+2:]
                            while curLine < endLine-1:
                                curLine = curLine+1
                                tex = tex + '\n' + block[curLine]
                            tex = tex + '\n' + block[endLine][:end]
                            curLine = endLine
                            line = block[curLine]
                            l = len(line)
                            l
                        if cur > last:
                            parts.append(StringPiece(line[last:cur]))
                        parts.append(TexPiece(tex))
                        last = end+2
                        cur = end+2
                    else:
                        cur = cur+1
                if cur > last:
                    parts.append(StringPiece(line[last:cur]))
                return (parts, curLine+1)
            except SongError: 
                raise
            except Exception:
                raise UnexpectedParsingError("while reading the line: "+ block[curLine])

        def getChords(line):
            """Take a line and identify if it is a chord line. If so, then
            return all chords as well as their positions in the line.
            Otherwise, return None"""
            try:
                words = line.split()
                # If almost all words are chords (at most two off, except if there
                # are only 1 or two chords), we continue.
                if len(list(filter(Chord.isChord, words))) >= max(len(words)-2, 1):
                    # Find indexes of chords
                    chords = []
                    i = 0
                    w = 0
                    line = line.rstrip()
                    l = len(line)
                    while i < l:
                        while i<l and line[i]==' ':
                            i=i+1
                        chords.append((Chord(words[w]), i))
                        w = w+1
                        while i<l and line[i]!=' ':
                            i=i+1
                    assert w == len(words)
                    return chords
                else:
                    return None
            except SongError as e: 
                e.info_stack.append(["While evaluating the chord line: %s" % line])
                raise
            except Exception:
                raise UnexpectedParsingError("while evaluating the chord line: "+ line)
            

        def parseLine(block, curLine):
            """Identify the type of the line (chords above, embedded or entire
            line) and parse it"""
            try:
                line = block[curLine]
                # If line is prepended with "play:", it is a chord line.
                if line[:5].lower() == "play:":
                    return ( ChordLine(line[5:].lstrip()) , curLine+1 )
                # If line starts with a paren, ends with one, and has none in between, it's a comment
                if line[0]=="(" and line[-1]==")" and not ("(" in line[1:-1]):
                    return ( CommentLine(line[1:-1]), curLine+1 )
                # We guess that curLine is a chord line. If it is not, getChords
                # should return None
                chords = getChords(line)
                if chords is None or curLine+1 >= len(block):
                    #Chords must be embedded in the line
                    (lineParts, nextLine) = parseLinePieces(block, curLine)
                    return (PiecesLine(lineParts), nextLine)
                else:
                    # Insert the chords in the following line and parse that using
                    # parseLinePieces. This reuses the code for parsing inlined TeX
                    nextLine = block[curLine+1]
                    def getChunk(start, end):
                        """Pick out a valid of nextLine. Almost always, this is
                        [start:end], but if end is in the middle of a TeX, it ends before"""
                        seeker = start
                        while seeker > -1:
                            occ = nextLine.find("{|",seeker)
                            if (occ > -1 and occ < end):
                                occEnd = nextLine.find("|}", occ)
                                if (occEnd == -1 or occEnd > end):
                                    end = occ
                                    seeker = -1
                                else:
                                    seeker=occEnd+1
                            else:
                                seeker = -1
                        return (nextLine[start:end], end)
                    newLine = []
                    last = 0
                    for (chord, pos) in chords:
                        (chunk, last) = getChunk(last, pos)
                        newLine.append(chunk)
                        newLine.append("("+ str(chord) + ")")
                    newLine.append(nextLine[last:])
                    block[curLine+1] = "".join(newLine)
                    (lineParts, nextLine) = parseLinePieces(block, curLine+1)
                    return (PiecesLine(lineParts), nextLine)
            except SongError as e:
                e.info_stack.append(["While looking at the line: " + block[curLine] ])
                raise
            except Exception as e:
                raise UnexpectedParsingError("while looking at the line: "+ block[curLine])
                        
        def parseVerse(block):
            # Parse a verse or a TexPart
            try:
                curLine = 0
                nline = len(block)
                verseLines = []
                while curLine < nline:
                    (verseLine, curLine) = parseLine(block, curLine)
                    verseLines.append(verseLine)
                # If only one verseLine with only Tex, then convert to TexPart
                if len(verseLines) == 1 \
                   and isinstance(verseLines[0], PiecesLine) \
                   and len(verseLines[0].pieces) == 1 \
                   and isinstance(verseLines[0].pieces[0], TexPiece):
                    return TexPart(verseLines[0].pieces[0].tex)
                return Verse(verseLines)
            except SongError as e:
                excerpts = grab_excerpts(block)
                e.info_stack.append(align_lines("While reading the verse:", excerpts))
                raise
            except Exception as e:
                excerpts = grab_excerpts(block)
                raise UnexpectedParsingError("while reading the verse:\n"+\
                                             align_lines(" "*8, excerpts))

        def parsePart(block):
            try:
                if (len(block)==1 and block[0][0] == "(" and block[0][-1]==")"):
                    return CommentPart(block[0][1:-1])
                if (block[0].lstrip())[:6].lower() == "chorus":
                    l = len(block)
                    if len(block[0]) == 6 and l == 1:
                        return Chorus()
                    elif len(block[0]) == 7 and block[0][6] == ":":
                        return ChorusDefinition(parseVerse(block[1:]))
                return parseVerse(block)
            except SongError as e:
                excerpts = grab_excerpts(block)
                e.info_stack.append(align_lines("While examining the block:", excerpts))
                raise
            except Exception as e:
                excerpts = grab_excerpts(block)
                raise UnexpectedParsingError("while examining the block:\n"+\
                                             align_lines(" "*8, excerpts))
            
        #parseStream Body 
        # The blocks are lines of the song file, separated by blank lines
        try:
            blocks = getBlocks()
        except Exception as e:
            raise UnexpectedParsingError("splitting up the song into blocks")

        try:
            # We assume that the first block is the meta data, the rest is the song
            extractMeta(blocks[0])
        except SongError:
            raise
        except Exception as e:
            raise UnexpectedParsingError("reading the meta data")

        try:
            self.parts = [ parsePart(block) for block in blocks[1:] ]
        except SongError:
            raise
        except Exception as e:
            raise UnexpectedParsingError("converting the individual blocks")

def align_lines(mes, lines):
    space = " "*len(mes)
    aligned = ["%s'%s'" % (mes, lines[0])]
    for l in lines[1:]:
        aligned.append("%s'%s'" % (space, l))
    return aligned

def grab_excerpts(block, lines=2):
    def grab_an_excerpt(line):
        return line[:20] + "..."
    if len(block) < lines:
        return [ grab_an_excerpt(l) for l in block ] + [""]*(lines-len(block))
    else:
        return [ grab_an_excerpt(l) for l in block[:lines] ]

def texString(s):
    """Replace backslashes with backslash symbol to sanitize strings not supposed to be TeX. Replace some other characters with pure TeX replacements"""
    chmap = dict([ (' ',"\\ "), ('&',"\\&") , ('\\',"\\textbackslash ") ,
                   ('~',"$\\textasciitilde$")
                 ])
    def mapc(char):
        if char in chmap:
            return chmap[char]
        else:
            return char
    slst = []
    for c in s:
        slst.append(mapc(c))
    return "".join(slst)

# vim: tw=0
